package com.dexciuq.stell_gallery.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.dexciuq.stell_gallery.data.DataSource
import com.dexciuq.stell_gallery.data.LocalDataSource
import com.dexciuq.stell_gallery.databinding.FragmentGalleryBinding
import com.dexciuq.stell_gallery.presentation.adapter.StellListAdapter

class GalleryFragment : Fragment() {

    private val binding by lazy { FragmentGalleryBinding.inflate(layoutInflater) }
    private val dataSource: DataSource = LocalDataSource

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val adapter = StellListAdapter(dataSource.getSteels()) { stell, extras ->
            findNavController().navigate(
                GalleryFragmentDirections.actionGalleryFragmentToStellFragment(
                    stell.id
                ), extras
            )
        }
        binding.stellRecyclerView.adapter = adapter

        postponeEnterTransition()
        binding.stellRecyclerView.doOnPreDraw {
            startPostponedEnterTransition()
        }
        return binding.root
    }
}