package com.dexciuq.stell_gallery.data

import com.dexciuq.stell_gallery.domain.Stell

interface DataSource {
    fun getSteels(): List<Stell>
}